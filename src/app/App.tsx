import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import HomePage from "./pages/home/HomePage";
import InfoPage from "./pages/info/InfoPage";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/info">
          <InfoPage />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
