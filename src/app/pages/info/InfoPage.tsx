import NavBar from "../../components/nav-bar/NavBar";

const InfoPage = () => (
  <div>
    <NavBar />
    <h1>Info Page</h1>
  </div>
);

export default InfoPage;
