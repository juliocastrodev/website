import NavBar from "../../components/nav-bar/NavBar";

const HomePage = () => (
  <div>
    <NavBar />
    <h1>HOME</h1>
  </div>
);

export default HomePage;
