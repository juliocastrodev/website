import { Link } from "react-router-dom";

const NavBar = () => (
  <ul>
    <li>
      <Link to="/">Home</Link>
    </li>
    <li>
      <Link to="/info">Info</Link>
    </li>
  </ul>
);

export default NavBar;
